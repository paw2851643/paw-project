@extends('layouts.main')

@section('main')
<div class="row">
  @include('partials.sideMenu')

  <div class="list-menu col">
    <div class="row">
      <form method="POST" action="{{ route('food.store') }}" enctype="multipart/form-data">
        @csrf
        <div class="mb-3">
          <label for="namaMenu" class="form-label w-25">Nama Menu</label>
          <input type="input" class="form-control w-25" id="namaMenu" name="name">
        </div>
        <div class="mb-3">
          <label for="harga" class="form-label">Harga</label>
          <input type="number" class="form-control w-25" id="harga" name="price">
        </div>
        <div class="input-group mb-3 w-25">
          <label class="input-group-text" for="Photo">Upload</label>
          <input type="file" class="form-control" id="Photo" name="photo">
        </div>

        <div class="input-group mb-3 w-25">
          <label class="input-group-text" for="inputGroupSelect01">Options</label>
          <select class="form-select" id="inputGroupSelect01" name="option">
            <option selected>Pilih Jenis Menu</option>
            <option value="Minuman">Minuman</option>
            <option value="Makanan">Makanan</option>
          </select>
        </div>

        <button type="submit" class="btn btn-primary mt-3">Submit</button>
      </form>
    </div>
  </div>
</div>
@endsection