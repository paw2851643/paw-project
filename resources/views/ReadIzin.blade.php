@extends('layouts.izinTemplate') 
@section('isi')
<div class="command">
    <h3>Perizinan</h3>
    <a href="/izin/create"><button class="btn btn-primary">Tambah</button></a>
</div>
<div class="container">
    <div class="content">
        @php
            $prevDate = null;
            $tableStarted = false;
            $tableCounter = 1;
        @endphp
        @foreach ($perizinan->sortBy('tanggal') as $izin)
            @php
                $currentDate = \Carbon\Carbon::parse($izin->tanggal)->format('d F Y');
            @endphp

            @if ($prevDate !== $currentDate)
                @if ($tableStarted)
                    </tbody>
                    </table>
                @endif
                <p>{{ $currentDate }}</p>
                <hr>
                <table class="table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Alasan</th>
                            <th>Surat</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php
                        $tableStarted = true;
                        $tableCounter = 1;
                    @endphp
                @endif
                <tr>
                    <td>{{ $tableCounter++ }}</td>
                    <td>{{ $izin->nama }}</td>
                    <td>{{ $izin->tipe }}</td>
                    <td>
                        @if ($izin->surat)
                            Ada
                        @else
                            Tidak
                        @endif
                    </td>
                    <td>
                        <a href="/izin/{{$izin->id}}/edit" class="btn btn-warning edit">Edit</a>
                        <form action="/izin/{{$izin->id}}" method="post" style="display:inline;">
                            @csrf
                            @method('delete')
                            <button type="submit" class="btn btn-danger"><span class="material-symbols-outlined">close</span></button>
                        </form>
                    </td>
                </tr>
                @php
                    $prevDate = $currentDate;
                @endphp
        @endforeach
    </div>
</div>
@endsection
