@extends('layout')
@section('title','Registration')
@section('content')
    <div class="container">
        <div class="mt-5 z-2">
            @if ($errors->any())
                <div class="col-12 position-relative">
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger">{{$error}}</div>
                    @endforeach
                </div>
            @endif

            @if (session()->has('error'))
                    <div class="alert alert-danger">{{session('error')}}</div>
            @endif

            @if (session()->has('success'))
            <div class="alert alert-success">{{session('success')}}</div>
    @endif
        </div>

        <div class="square-reg z-n1 position-absolute top-48 start-50 translate-middle"></div>  
        <div class="position-sticky">
            <form action="{{route('registrationPost')}}" method="POST" class="ms-auto me-auto mt-c" style="width:500px">
            @csrf
           <h1>Register</h1>
            {{-- Email --}}
            <div class="mb-2 py-1">
              <label class="form-label-reg">Email</label>
              <input type="email" class="form-control" name="email" required>
            </div>
            {{-- NAMA --}}
            <div class="mb-2 pb-1">
                <label class="form-label-reg">Nama Lengkap</label>
                <input type="text" class="form-control" name="nama">
            </div>
            {{-- NIK --}}
            <div class="mb-2 pb-1">
                <label class="form-label-reg">NIK</label>
                <input type="text" class="form-control" name="nik">            
            </div>
            {{-- NIK --}}
            <div class="mb-2 pb-1">
                <label class="form-label-reg">Nomor HandPhone</label>
                <input type="text" class="form-control" name="NoHP">            
            </div>
            {{-- GENDER --}}
            <div class="mb-2 pb-1">
            <label for="Gender" class="form-label-reg">Gender</label>
            <br>
                <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="gender" id="gender1" value="Pria" >
                <div class="form-check-label" for="gender1">Pria</div>
                </div>
                <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="gender" id="gender2" value="Perempuan">
                <div class="form-check-label" for="gender2">Perempuan</div>
                </div>
            </div>  
            {{-- Password --}}
            <div class="mb-2 pb-1">
              <label class="form-label-reg">Password</label>
              <input type="password" class="form-control" name="password">
            </div>
            {{-- konfirmasi password  --}}
            <div class="mb-2 pb-1">
                <label class="form-label-reg">Konfirmasi Password</label>
                <input type="password" class="form-control" name="Kpassword" >
              </div>
              {{-- setuju --}}
            {{-- <div class="mb-2 form-check">
              <input type="checkbox" class="form-check-input" >
              <label class="form-check-label" for="exampleCheck1">Check me out</label>
            </div> --}}
            {{-- submit --}}
            <div class="py-2">
            <button type="submit" class="btn btn-login btn-lg">Register</button>
            <div id="emailHelp" class="form-text">
                <a href="/login">Sudah punya akun?
                </a>
                </div>
            </div>
            </div>
          </form>
    </div>
@endsection