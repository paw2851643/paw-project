@extends('layouts.izinTemplate')
@section('isi')

<h3 class="m-3">Edit Perizinan</h3>
<form action="/izin/{{$izin->id}}" method="post" enctype="multipart/form-data">
    @method('put')
    @csrf
  <div class="container">
    <div class="form-container">
        <div class="form-group">
        <label for="nama">Name:</label>
        <input type="text" id="nama" name="nama" class="form-control" value="{{$izin->nama}}" readonly>
      </div>
      <div class="form-group">
        <label for="tanggal">Date:</label>
        <input type="date" id="tanggal" name="tanggal" class="form-control" value="{{$izin->tanggal}}">
      </div>

      <div class="form-group">
        <label for="tipe">Type of Permission:</label>
        <select class="form-select" id="tipe" name="tipe">
          <option value="sakit" @if($izin->tipe == 'Sakit') selected @endif>Sakit</option>
          <option value="izin" @if($izin->tipe == 'Izin') selected @endif>Izin</option>
        </select>
      </div>

      <div class="form-group">
        <!-- File input and drop area -->
        <div class="file-drop-area" id="file-drop-area">
          <input type="file" id="file-input" name="surat" class="file-input" onchange="displayFileName()">
          <label for="file-input" class="file-input-label">
            <span class="material-symbols-outlined icon">draft</span>
            <p>Drag and drop files here <br>or click to browse</p>
          </label>
          <div id="file-name" class="file-name"></div>
        </div>
      </div>

      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </div>
</form>

<script>
function displayFileName() {
    var input = document.getElementById('file-input');
    var fileName = input.files[0].name;
    // Remove "post-image/" from the file name
    var cleanedFileName = fileName.replace('post-image/', '');
    document.getElementById('file-name').innerHTML = 'File: ' + cleanedFileName;
    document.querySelector('.file-input-label').style.display = 'none';
}

document.addEventListener('DOMContentLoaded', function() {
    var fileNameFromDatabase = '{{ $izin->surat }}';
    if (fileNameFromDatabase) {
        // Remove "post-image/" from the file name
        var cleanedFileName = fileNameFromDatabase.replace('post-image/', '');
        document.getElementById('file-name').innerHTML = 'File: ' + cleanedFileName;
        document.querySelector('.file-input-label').style.display = 'none';
    }
});
</script>

@endsection
