@extends('layouts.main')

@section('main')
<div class="row">
  @include('partials.sideMenu')

  <div class="list-menu col">
    <div class="row">

        <div class="col-md-4">
          <div class="menu rounded-2" id="menu">
            <div class="row">
              <div class="foto col-md-3 rounded-circle"></div>
              <div class="makanan col-md-6">
                  <a href="/update/updateFood" class="text-decoration-none text-dark">
                  <h6>Makanan</h6>
                </a>
                </div>

              </div>
            </div>
          </div>

        <div class="col-md-4">
          <div class="menu rounded-2" id="menu">
            <div class="row">
              <div class="foto col-md-3 rounded-circle"></div>
              <div class="makanan col-md-6">
                  <a href="/update/updateDrinks" class="text-decoration-none text-dark">
                  <h6>Minuman</h6>
                </a>
                </div>

              </div>
            </div>
          </div>

    </div>
  </div>
</div>
@endsection
