@extends('layouts.atas')
@section('title','Navbar_test')
@section('navbar')


<nav class="navbar navbar-custom navbar-expand-lg navbar-nav">
    <div class="container-fluid">
      <a class="navbar-brand fs-1 px-3c" href="/">Restaurant</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse mx-auto" id="navbarSupportedContent">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link active fs-4 px-3b" aria-current="page" href="/">Menu</a>
          </li>
          <li class="nav-item">
            <a class="nav-link fs-4 px-3b" href="/izin">Perizinan</a>
          </li>
        </ul>
        <div class="navbar-custom">
          <form action="{{ route('profile.show', auth()->user()) }}" class="form-check form-check-inline">
          <button class="btn btn-outline-success prof" type="submit">Profile</button>
        </form>
          <form action="{{route('Logout')}}" class="form-check form-check-inline">
            @csrf 
          <button class="btn btn-outline-danger" type="submit">Logout</button>
        </form>
        </div>
      </div>
    </div>
  </nav>
  @endsection