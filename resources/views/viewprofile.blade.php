@extends('partials.navbar')
@section('content')

	<div>
		<div class="container mt-5">
			<div class="main-body">
				<div class="row">
					<div class="col-lg-4 ">
						<div class="card ">
							<div class="card-body">
								<div class="d-flex flex-column align-items-center text-center">
									<img src="{{asset('foto/'.$profile->foto)}}" alt="Admin" class="rounded-circle p-1 " height="200" width="200">
									<div class="mt-3">
										<h4>{{$profile->nama}}</h4><br>
										<div class="row mb-3">
											<div class="col-5">
												<h6 class="mb-0 text-start">Shift :</h6>
											</div>
											<div class="col-sm-6 text-start">
												<h6>{{$profile->shift}}</h6>
											</div>
											<div class="col-5">
												<h6 class="mb-0 text-start">Nomor HP :</h6>
											</div>
											<div class="col-sm-6 text-start">
												<h6>{{$profile->NoHP}}</h6>
											</div>
										</div>
									</div>
								</div>
								<hr class="my-4">
							</div>
						</div>
					</div>
					<div class="col-lg-8">
						<div class="card">
							<div class="card-body">
								<form action="/profile/{{$profile->id}}" method="post" enctype="multipart/form-data">
								@csrf
								@method('PUT')
									<h5>Ubah Biodata Diri</h5>
									<div class="row mb-3">
										<div class="col-sm-3">
											<h6 class="mb-0">Nama lengkap :</h6>
										</div>
										<div class="col-sm-9 text-secondary">
											<input type="text" class="form-control" name="nama" id="nama" value="{{$profile->nama}}" >
										<br></div>
									</div>
									<div class="row mb-3">
										<div class="col-sm-3">
											<h6 class="mb-0">Shift :</h6>
										</div>
										<div class="col-sm-9 text-secondary">
											<input type="text" class="form-control" name="shift" id="shift" value="{{$profile->shift}}">
										<br></div>
									</div>
									<div class="row mb-3">
										<div class="col-sm-3">
											<h6 class="mb-0">Nomor HP :</h6>
										</div>
										<div class="col-sm-9 text-secondary">
											<input type="number" class="form-control" name="NoHP" id="NoHP" value="{{$profile->NoHP}}">
										</div>
									</div>
									<input class="" type="file" name= "foto">
									<div class="row">
										<div class="col-sm-3"></div>
										<div class="col-sm-9 text-secondary">
											<br><input type="submit" class="btn btn-primary px-4" value="Simpan Perubahan">
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@extends('layouts.bawah')



