@extends('layout')
@section('title','Login')
@section('content')
    <div class="container">
      <div class="mt-5">
        @if ($errors->any())
            <div class="col-12">
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger">{{$error}}</div>
                @endforeach
            </div>
        @endif
        @if (session()->has('error'))
                <div class="alert alert-danger">{{session('error')}}</div>
        @endif
        @if (session()->has('success'))
        <div class="alert alert-success">{{session('success')}}</div>
@endif

<div class="position-absolute top-23 start-58 translate-middle">
  <div class="slide">
  <div id="carouselExampleAutoplaying" class="carousel slide carousel-fade" data-bs-ride="carousel" data-bs-interval="5000">
 <div class="carousel-inner">
   <div class="carousel-item active">
     <img src="{{ asset('image/pizza.png') }}" alt="pizza">
    </div>
    <div class="carousel-item">
      <img src="{{ asset('image/mie.png') }}" alt="mie">
    </div>
    <div class="carousel-item">  
      <img src="{{ asset('image/batagor.png') }}" alt="batagor">
    </div>
    <div class="carousel-item">
      <img src="{{ asset('image/lasagna.png') }}" alt="lasagna">
    </div>
  </div>
  </div>
    </div>
      </div>
{{-- square --}}
<div class="square z-n1 position-absolute top-45 start-50 translate-middle"></div>  
        <div class="semua">
        <form action="{{route('loginPost')}}" method="POST" class="ms-auto me-auto mt-5 position-absolute top-40 start-46 translate-middle" style="width:500px">
          @csrf
          <h1 class="mb-4">Login</h1>
          
          <div class="mb-3 col-8 ms-0">
              <label class="form-label ms-2 mb-1">Email address</label>
              <input type="email" class="form-control" name="email">
              <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
            </div>
            <div class="mb-3 col-8 ms-0">
              <label class="form-label ms-2 mb-1">Password</label>
              <input type="password" class="form-control" name="password">
            </div>
            <div class="mt-6 ms-0">
            <button type="submit" class="btn btn-login btn-lg">Login</button>
          </div>
        </form>
        <div class="mx-custom mt-6 ms-0 ms-auto me-auto mt-5 position-absolute top-55 start-38 translate-middle ">
          <form action="{{route('registration')}}">
            <button type="submit" class="btn btn-register btn-lg ">Register</button>
          </form>
      </div>
        </div>
    </div>
@endsection