@extends('layouts.main')

@section('main')
<div class="row">
  @include('partials.sideMenu')

  <div class="list-menu col">
    <div class="row">
        @foreach ($makanan as $item)
        <div class="col-md-4">
          <div class="menu rounded-2 mb-4" id="menu">
            <div class="row">
            <img src="{{ asset('storage/' . $item->photo) }}" alt="" class="foto col-md-3 rounded-circle p-0">
              <div class="makanan col-md-6">
                  <a href="/update/updateFood/{{ $item["id"] }}" class="text-decoration-none text-dark">
                  <h6>{{ $item['name'] }}</h6>
                  <p>Rp {{ $item['price'] }}</p>
                </a>
                </div>
              </div>
            </div>
          </div>
        @endforeach
    </div>
  </div>
</div>
@endsection
