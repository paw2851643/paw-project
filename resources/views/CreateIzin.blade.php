@extends('layouts.izinTemplate')
@section('isi')
<h3 class="m-3">Perizinan</h3>
<form action="/izin" method="post" enctype="multipart/form-data">
    @csrf
  <div class="container">
    <div class="form-container">
      <div class="form-group">
        <label for="nama">Name:</label>
        <select id="nama" name="nama" class="form-select">
        @foreach($user as $user)
            <option value="{{ $user->nama }}">{{ $user->nama }}</option>
        @endforeach
        </select>
      </div>
      <div class="form-group">
        <label for="tanggal">Date:</label>
        <input type="date" id="tanggal" name="tanggal" class="form-control">
      </div>

      <div class="form-group">
        <label for="tipe">Type of Permission:</label>
        <select class="form-select" id="tipe" name="tipe">
          <option selected="sakit">Sakit</option>
          <option value="izin">Izin</option>
        </select>
      </div>
      
      <div class="form-group">
        <!-- File input and drop area -->
        <div class="file-drop-area" id="file-drop-area">
          <input type="file" id="file-input" name="surat" class="file-input" onchange="displayFileName()">
          <label for="file-input" class="file-input-label">
            <span class="material-symbols-outlined icon">draft</span>
            <p>Drag and drop files here <br>or click to browse</p>
          </label>
          <div id="file-name" class="file-name"></div>
        </div>
      </div>

      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </div>
</form>

<script>
function displayFileName() {
    var input = document.getElementById('file-input');
    var fileName = input.files[0].name;
    document.getElementById('file-name').innerHTML = 'File: ' + fileName;
    document.querySelector('.file-input-label').style.display = 'none';
}
</script>

@endsection
