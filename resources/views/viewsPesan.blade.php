@extends('layouts.main')

@section('main')
<div class="row">
    @include('partials.sideMenu')

    <div class="list-menu col">
        <div class="row">
            <table class="table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Harga</th>
                        <th>Jumlah</th>
                        <th>Total Harga</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($orders as $index => $order)
                    <tr>
                        <td>{{ $index + 1 }}</td>
                        <td>{{ $order->name }}</td>
                        <td>Rp {{ $order->price }}</td>
                        <td>{{ $order->qty }}</td>
                        <td>Rp {{ $order->price * $order->qty }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="row">
                    <h3 class="col-9 text-start">Total Harga:</h3>
                    <h3 class="col-3 text-start">Rp {{ $total }}</h3>
            </div>
            <div class="fixed-bottom d-flex justify-content-end mb-3 me-3">
                <!-- Tombol "Selesai" -->
                <form action="{{ route('finish.order') }}" method="POST">
                    @csrf
                    <button type="submit" class="btn btn-primary btn-lg">Selesai</button>
                </form>
            </div>  
        </div>
    </div>
</div>
@endsection