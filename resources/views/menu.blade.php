@extends('layouts.main')

@section('main')
<div class="row">
  @include('partials.sideMenu')

  <div class="list-menu col">
    <div class="row">
      @foreach ((($title) === "Menu | Makanan" ? $makanan : $minuman) as $item)
      <div class="col-md-4">
        <button class="menu-btn col-md-12 rounded-2 mb-4" style="border: none;" data-bs-toggle="modal"
          data-bs-target="#exampleModal{{ $loop->index }}">
          <div class="menu rounded-2" id="menu">
            <div class="row">
              <img src="{{ asset('storage/' . $item->photo) }}" alt="" class="foto col-md-3 rounded-circle p-0">
              <div class="makanan col-md-6 text-start">
                <h6>{{ $item['name'] }}</h6>
                <p>Rp {{ $item['price'] }}</p>
              </div>
            </div>
          </div>
        </button>
      </div>

      <!-- Modal -->
      <div class="modal fade" id="exampleModal{{ $loop->index }}" tabindex="-1" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <form action="{{ route('orders.store') }}" method="POST">
              @csrf
              <input type="hidden" name="id" value="{{ $item['id'] }}">
              <div class="modal-header row mx-2">
                <h5 class="modal-title w-50" id="exampleModalLabel">Pesan</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-body">
                <h6 class="mb-0">Nama</h6>
                <input name="name" type="text" class="rounded-2 mb-1" value="{{ $item['name'] }}" readonly>
                <h6 class="mb-0">Harga</h6>
                <input name="price" type="text" class="rounded-2 mb-1" value="{{ $item['price'] }}" readonly>
                <h6 class="mb-0">Jumlah</h6>
                <input name="qty" type="text" class="rounded-2">
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
            </form>
          </div>
        </div>
      </div>
      @endforeach
    </div>
  </div>
</div>
@include('partials.check')
@endsection

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>