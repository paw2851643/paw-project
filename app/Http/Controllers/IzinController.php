<?php

namespace App\Http\Controllers;

use App\Models\Perizinan;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class IzinController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('ReadIzin',[
            'title' => 'Perizinan',
            'style' => 'read',
            'perizinan' => Perizinan::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(User $user)
    {
        return view('CreateIzin',[
            'title' => 'Perizinan | Create',
            'style' => 'create',
            'user' => User::all(),
        ]);

    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'nama' => 'required',
            'tanggal' => 'required',
            'tipe' => 'required',
            'surat' => 'image|file|max:5024'
        ]);

        if ($request->file('surat')) {
            $validateData['surat'] = $request->file('surat')->store('post-image');
        }

        Perizinan::create($validateData);
        return redirect('/izin');
    }

    /**
     * Display the specified resource.
     */
    public function show(Perizinan $izin)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Perizinan $izin)
    {
        return view('UpdateIzin',[
            'title' => 'Perizinan | Create',
            'style' => 'create',
            'izin' => $izin
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Perizinan $izin)
    {
    $rule = [
        'nama' => 'required',
        'tanggal' => 'required',
    ];

    if ($request->tipe != $izin->tipe) {
        $rule['tipe'] = 'required';
    }

    $validateData = $request->validate($rule);
    Perizinan::where('id',$izin->id)
                ->update($validateData);
    return redirect('/izin');
    }

    public function destroy(Perizinan $izin)
    {
        if ($izin->surat) {
        Storage::delete($izin->surat);
    }
        Perizinan::destroy($izin->id);
        return redirect('/izin');
    }
}
