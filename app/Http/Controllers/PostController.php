<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Drinks;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    public function index() {
        if  (Auth::check()){
            return view('menu', [
                "title" => "Menu | Makanan",
                "makanan" => Post::all()
            ]);
        }
        return view('Login');
    }
    

    public function drinks() {
        if  (Auth::check()){
            return view('menu', [
                "title" => "Menu | Minuman",
                "minuman" => Drinks::all()
            ]);
        }
        return view('Login');
        }

    public function add() {
        if  (Auth::check()){
            return view('add', [
                "title" => "Menu | Tambah Menu",
            ]);
        }
        return view('Login');
    }

    public function update() {
        if  (Auth::check()){
            return view('update', [
                "title" => "Update Menu",
                "makanan" => Post::all(),
                "minuman" => Drinks::all()
            ]);
        }
        return view('Login');
    }

    public function updateFood() {
        if  (Auth::check()){

            return view('updateFood', [
                "title" => "Update Menu Makanan",
                "makanan" => Post::all()
            ]);
        }
        return view('Login');
        }

    public function updateDrinks() {
        if  (Auth::check()){

            return view('updateDrinks', [
                "title" => "Update Menu Minuman",
                "minuman" => Drinks::all()
            ]);
        }
        return view('Login');
    }


    public function store(Request $request) {
        $request->validate([
            'photo' => 'required|image|max:2048',
            // tambahkan validasi sesuai kebutuhan, seperti nama menu, harga, dll.
        ]);
    
        $photoPath = $request->file('photo')->store('menu-images');
    
        if ($request->input('option') === "Makanan") {
            Post::create(array_merge($request->except('photo'), ['photo' => $photoPath]));
            return redirect("/menu");
        } elseif ($request->input('option') === "Minuman") {
            Drinks::create(array_merge($request->except('photo'), ['photo' => $photoPath]));
            return redirect("/drinks");
        }
    }
    

    public function show($id) {
        if  (Auth::check()){
            $makanan = Post::find($id);

            return view('updateMenu', [
                "title" => "Update Menu Makanan",
                "makanan" => $makanan
            ]);
        }
        return view('Login');
    }

    public function showDrinks($id) {
        if  (Auth::check()){
            $minuman = Drinks::find($id);

            return view('updateMenu', [
                "title" => "Update Menu Minuman",
                "minuman" => $minuman
            ]);
        }
        return view('Login');
    }
    
    public function delete($id) {
        if  (Auth::check()){
            $makanan = Post::find($id);
            $makanan->delete();
            return redirect("/update/updateFood");
        }
        return view('Login');
    }

    public function deleteDrinks($id) {
        if  (Auth::check()){

            $minuman = Drinks::find($id);
            
            $minuman->delete();
            return redirect("/update/updateDrinks");
        }
        return view('Login');
    }

    public function updateMakanan(Request $request, $id) {
        if  (Auth::check()){

            $makanan = Post::where('id', $id)->first()->update($request->all());
            // $makanan = Post::find($request);
            // $makanan->update($request);
            
            return redirect("/update/updateFood");
        }
        return view('Login');
    }
    public function updateMinuman(Request $request, $id) {
        if  (Auth::check()){

            $minuman = Drinks::where('id', $id)->first()->update($request->all());
            // $minuman = Drinks::find($id);
            // $minuman->update($request);
            
            return redirect('/update/updateDrinks');
        }
        return view('Login');
    }
}
