<?php

namespace App\Http\Controllers;
use App\Models\Profie;
use App\Models\User;
use Illuminate\Http\Request;

class PController extends Controller{
   
    public function index()
    {
        return view('home',[
            'profile' => User::all()
        ]);
    }

    
    public function create()
    {
        
    }

   
    public function store(Request $request)
    {
        
    }


    public function show(User $profile)
    {
        return view('viewprofile', [
            'profile' => $profile
        ]);
    }

    public function edit(User $profile)
    {
        return view('viewprofile',[
            'profile' => $profile
        ]);
    }

 
    public function update(Request $request, User $profile){
       
    $rules = [
        'nama' => 'required',
        'NoHP' => 'required',
    ];


    if ($request->shift != $profile->shift) {
        $rules['shift'] = 'required';
    }
    
    $validatedData = $request->validate($rules);

    if ($request->hasFile('foto')) {
        $fotoPath = $request->file('foto');
        $ubah_nama = time().$fotoPath->getClientOriginalName();
        $fotoPath->move('foto', $ubah_nama);
        $profile->foto=$ubah_nama;
        $profile->save();
       
    }

    $profile->update($validatedData);

    return redirect()->route('profile.show', $profile)->with('success', 'Profil berhasil diperbarui');
  
    }


 
    public function destroy(string $id)
    {
        
    }
   
}
