<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{

    public function store(Request $request)
    {
        $total = $request->price * $request->qty;

        $order = Order::create([
            'name' => $request->name,
            'price' => $request->price,
            'qty' => $request->qty,
            'total' => $total,
        ]);

        return redirect()->back()->with('success', 'Pesanan berhasil disimpan');
    }


    public function total()
    {
        if  (Auth::check()){

            $total = Order::select(DB::raw('SUM(price * qty) AS total_price'))->first()->total_price;
            
            return $total;
        }
        return view('Login');
    }

    public function check()
    {
        if  (Auth::check()){

            $orders = Order::all();
            
            $total = DB::table('orders')->sum(DB::raw('price * qty'));
            
            return view('viewsPesan', ['orders' => $orders, 'total' => $total, "title" => "Cek Total"]);
        }
        return view('Login');
    }

    public function finishOrder(Request $request)
    {
        if  (Auth::check()){

            // Delete or truncate the orders data
            Order::truncate();
            
            // Redirect back to the menu page with a success message
            return redirect('/');
        }
        return view('Login');
    }
}