<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Auth\AuthManager as AuthAuthManager;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;


class AuthManager extends Controller
{
    
    function nav(){
        return view('navbar');
    }
    function login(){
        if  (Auth::check()){
            return redirect(route('index'));
        }
        return view('Login');
    }
    function registration(){
        if  (Auth::check()){
            return redirect(route('index'));
        }
        return view('registration');
    }
    function loginPost(Request $request){
        $request->validate([
            'email' => 'required',
            'password' => 'required'
        ]);
    
        $credentials = $request->only('email','password');
        if(Auth::attempt($credentials)){
            return redirect()->intended(route('index'));
        }
        return redirect(route('Login'))->with("error","Login details are not valid");
    }
    function registrationPost(Request $request){
        $request->validate([
            'email' => 'required|email|unique:users',
            'nama' => 'required',
            'nik' => 'required',
            'NoHP'=> 'required',
            'gender' => 'required',
            'password' => 'required|required_with:Kpassword|min:6',
            'Kpassword' => 'required|min:6|same:password'
        ]);    
        $data['nama']=$request->nama;
        $data['email']=$request->email;
        $data['nik']=$request->nik;
        $data['NoHP']=$request->NoHP;
        $data['gender']=$request->gender;
        $data['password']=Hash::make($request->password);
        $user= User::create($data);
        if(!$user){
            return redirect(route('registration'))->with("error","Registration Failed");
        }
        return redirect(route('Login'))->with("success","Registration succesfull");
    }
    
    function logout(){
        Session::flush();
        Auth::logout();
        return redirect(route('Login')); 
    }
    protected $redirectTo = '/login';

}
