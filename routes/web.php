<?php

use App\Http\Controllers\PController;
use App\Http\Controllers\AuthManager;
use App\Http\Controllers\ProfileController;
use Illuminate\Foundation\Application;
use App\Http\Controllers\IzinController;
use App\Models\Post;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Http\Controllers\PostController;
use App\Http\Controllers\OrderController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::resource('/profile', PController::class,);

Route::get('/profile/{profile}', [PController::class, 'show'])->name('profile.show');
Route::get('/profile/{profile}/edit', [PController::class, 'edit'])->name('profile.edit');
Route::put('/profile/{profile}', [PController::class, 'update'])->name('profile.update');


Route::put('update/updateFood/{menu}/updateFood', [PostController::class, 'updateMakanan'])->name('food.update');


Route::get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    // Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

Route::get('/login',[AuthManager::class,'login'])->name('Login');
Route::post('/login',[AuthManager::class,'loginPost'])->name('loginPost');
Route::get('/registration',[AuthManager::class,'registration'])->name('registration');
Route::post('/registration',[AuthManager::class,'registrationPost'])->name('registrationPost');
Route::get('/logout',[AuthManager::class,'logout'])->name('Logout');
Route::get('/nav',[AuthManager::class,'nav'])->name('Test');
Route::get('/', function () {
    return redirect('/login');
});

Route::resource('/izin', IzinController::class);

require __DIR__.'/auth.php';

Route::get('/menu', [PostController::class, 'index'])->name('index');;
Route::get('/drinks', [PostController::class, 'drinks']);
Route::get('/add', [PostController::class, 'add']);
Route::get('/update', [PostController::class, 'update']);
Route::get('/update/updateFood', [PostController::class, 'updateFood']);
Route::get('/update/updateDrinks', [PostController::class, 'updateDrinks']);
Route::get('/update/updateFood/{menu}', [PostController::class, 'show']);
Route::get('/update/updateDrinks/{menu}', [PostController::class, 'showDrinks']);
Route::post('/add/create', [PostController::class, 'store'])->name('food.store');
Route::get('update/updateFood/{menu}/deleteFood', [PostController::class, 'delete'])->name('food.delete');
Route::get('update/updateDrinks/{menu}/deleteDrink', [PostController::class, 'deleteDrinks'])->name('drinks.delete');
Route::put('update/updateFood/{menu}/updateFood', [PostController::class, 'updateMakanan'])->name('food.update');
Route::put('update/updateDrinks/{menu}/updateDrinks', [PostController::class, 'updateMinuman'])->name('drinks.update');
Route::post('/orders', [OrderController::class, 'store'])->name('orders.store');
Route::get('/check', [OrderController::class, 'check'])->name('check.order');
Route::post('/finish', [OrderController::class, 'finishOrder'])->name('finish.order');

